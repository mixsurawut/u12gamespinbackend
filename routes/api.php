<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Token;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('save_select_match', 'MatchSelectController@store');
Route::get('migrate', function () {
    return Artisan::call('migrate');
});
Route::get('link', function () {
    return Artisan::call('storage:link');
});



Route::get('update/fbToken', function () {
    $curl = curl_init();
    $token = Token::whereName('facebook')->first();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=793782051572248&client_secret=a12cdd297fb11422e53c1056e7a0b5a4&fb_exchange_token={$token->val}",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    $data = json_decode($response, true);
    $token->update([
        'val' => $data['access_token']
    ]);
    curl_close($curl);
    return $data;
});
