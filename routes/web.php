<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Models\Wheel;

// Route::get('profile', 'ProfileController@index');
Route::get('admin/userdata', 'ProfileController@index');

Auth::routes();


Route::prefix('login')->group(function () {
    Route::get('/{provider}', 'Auth\LoginController@redirectToProvider')->name('login.provider');
    Route::get('/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.provider.callback');
});


Route::get('/', 'HomeController@index')->name('home');

Route::middleware(['admin'])->group(function () {
    Route::resource('admin/codes', 'Admin\CodesController');
    Route::resource('admin/items', 'Admin\ItemsController');
    Route::resource('admin/rewards', 'Admin\RewardController');
    // Route::resource('admin/wheel', 'Admin\WheelController');
});

Route::middleware(['superadmin'])->group(function () {
    Route::resource('admin/wheel', 'Admin\WheelController');
});

Route::resource('my_rewards', 'MyRewardController');

Route::post('redeems/use', 'RedeemController@use');
Route::resource('redeems', 'RedeemController');
Route::resource('address', 'AddressController');
Route::resource('rewards', 'RewardController');

Route::get('wheel', function () {

    $user = request()->user();

    $count = $user ? count($user->redeems) : 0;

    $wheels = Wheel::get();

    foreach ($wheels as $wheel) {
        $wheel->resultText = $wheel->result_text;
    }

    $jayParsedAry = [
        "colorArray" => [
            "#4E4AFF",
            "#1A5BA3",
            "#0083AC",
            "#67B6FF",
            "#568FFD",
            "#4E4AFF",
            "#1A5BA3",
            "#0083AC",
            "#67B6FF",
            "#568FFD",
            "#4E4AFF",
            "#1A5BA3",
            "#0083AC",
            "#67B6FF",
            "#568FFD",
            "#4E4AFF",
            "#1A5BA3",
            "#0083AC",
            "#67B6FF",
            "#568FFD",
            "#4E4AFF",
            "#1A5BA3",
            "#0083AC",
            "#67B6FF",
            "#568FFD",
        ],
        "segmentValuesArray" => $wheels,
        "svgWidth" => 1024,
        "svgHeight" => 1024,
        "wheelStrokeColor" => "#95c8ff",
        "wheelStrokeWidth" => 18,
        "wheelSize" => 900,
        "wheelTextOffsetY" => 80,
        "wheelTextColor" => "#EDEDED",
        "wheelTextSize" => "2.3em",
        "wheelImageOffsetY" => 40,
        "wheelImageSize" => 50,
        "centerCircleSize" => 360,
        "centerCircleStrokeColor" => "#114072",
        "centerCircleStrokeWidth" => 12,
        "centerCircleFillColor" => "#EDEDED",
        "centerCircleImageUrl" => "https://u12game.com/wp-content/uploads/2020/11/s3.png",
        "centerCircleImageWidth" => 400,
        "centerCircleImageHeight" => 400,
        "segmentStrokeColor" => "#95c8ff",
        "segmentStrokeWidth" => 4,
        "centerX" => 512,
        "centerY" => 512,
        "hasShadows" => false,
        "numSpins" => $count,
        "spinDestinationArray" => [],
        "minSpinDuration" => 3,
        "gameOverText" => "ขอบคุณที่มาร่วมสนุก",
        "invalidSpinText" => "INVALID SPIN. PLEASE SPIN AGAIN.",
        "introText" => "กรุณาหมุนวงล้อเพื่อลุ้นของรางวัล",
        "hasSound" => true,
        "gameId" => "9a0232ec06bc431114e2a7f3aea03bbe2164f1aa",
        "clickToSpin" => true,
        "spinDirection" => "ccw"
    ];
    return $jayParsedAry;
});
