

<div class="form-group <?php echo e($errors->has('cash') ? 'has-error' : ''); ?>">
    <label for="cash" class="control-label"><?php echo e('จำนวนเงินที่เติม'); ?></label>
    <input class="form-control" name="cash" type="text" id="cash" value="<?php echo e(isset($code->cash) ? $code->cash : ''); ?>" required>
    <?php echo $errors->first('cash', '<p class="help-block">:message</p>'); ?>

</div>



<div class="form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? 'Update' : 'สุ่ม'); ?>">
</div>
<?php /**PATH C:\xampp\htdocs\u12gamespinbackend\resources\views/admin/codes/form.blade.php ENDPATH**/ ?>