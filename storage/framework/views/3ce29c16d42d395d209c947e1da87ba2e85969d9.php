<?php $__env->startSection('style'); ?>
<link rel="stylesheet" href="/css/style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet"
    crossorigin="anonymous">
<link
    href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap"
    rel="stylesheet">
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<style>
    html,
    body {
        font-family: 'Kanit', sans-serif !important;
    }
</style>

<div class="container" style="min-height: 700px;">
    <div class="card p-4 shadow">
        <h3 class="mt-4 mb-5">กรุณากรอกข้อมูลของการจัดส่ง</h3>
        <form action="/address" method="POST">
                            <?php echo e(csrf_field()); ?>

            <div class="row">
                <div class="col-md-6 m-auto">
                    <div class="input-group mb-3">
                        <input required type="text" class="form-control" placeholder="ชื่อ-นามสกุล" name=
                        "full_name" aria-label="full_name" value="<?php echo e($user->full_name); ?>"
                            aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3">
                        <textarea  style="min-height:100px;" required type="text" class="form-control" placeholder="ที่อยู่" name=
                        "address" aria-label=""
                            aria-describedby="basic-addon1"><?php echo e($user->address); ?></textarea>
                    </div>
                    <div class="input-group mb-3">
                        <input required type="text" class="form-control" placeholder="รหัสไปรษณีย์" name=
                        "post_code" aria-label="post_code" value="<?php echo e($user->post_code); ?>"
                            aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3">
                        <input required type="text" class="form-control" placeholder="เบอร์โทร" name=
                        "phone" aria-label="phone" value="<?php echo e($user->phone); ?>"
                            aria-describedby="basic-addon1">
                    </div>

                    <p class="text-muted">* กรุณาตรวจสอบข้อมูลให้ครบถ้วน เพื่อไม่ให้เสียสิทธิในการรับรางวัล *</p>

                    <button class="btn btn-outline-primary my-5" type="submit">ยืนยัน</button>

                </div>
            </div>

        </form>

    </div>

</div>

<footer>u12game.com</footer>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>

</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/utils/Draggable.min.js'></script>
<script src='/js/ThrowPropsPlugin.min.js'></script>
<script src='/js/Spin2WinWheel.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/TextPlugin.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"
    integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"
    iegrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

<script src="/js/index.js"></script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\u12gamespinbackend\resources\views/address.blade.php ENDPATH**/ ?>