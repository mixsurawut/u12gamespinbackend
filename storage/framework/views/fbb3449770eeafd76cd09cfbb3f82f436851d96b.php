<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('Name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" value="<?php echo e(isset($item->name) ? $item->name : ''); ?>" required>
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>
<div class="form-group <?php echo e($errors->has('image_url') ? 'has-error' : ''); ?>">
    <label for="image_url" class="control-label"><?php echo e('Image Url'); ?></label>
    <input class="form-control" name="image_url" type="file" id="image_url" value="<?php echo e(isset($item->image_url) ? $item->image_url : ''); ?>" required>
    <?php echo $errors->first('image_url', '<p class="help-block">:message</p>'); ?>

</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? 'Update' : 'Create'); ?>">
</div>
<?php /**PATH C:\xampp\htdocs\u12gamespinbackend\resources\views/admin/items/form.blade.php ENDPATH**/ ?>