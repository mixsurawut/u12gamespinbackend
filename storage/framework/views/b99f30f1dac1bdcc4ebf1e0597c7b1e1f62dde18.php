<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Menu
        </div>

        <div class="card-body">
            <!-- <ul role="tablist">
                <li role="presentation">

                </li>
                <li role="presentation">
                    <a href="">

                    </a>
                </li>
                <li role="presentation">
                    <a href="">

                    </a>
                </li>

            </ul> -->
            <nav class="nav flex-column">
                <a class="nav-link" href="<?php echo e(url('/admin/codes')); ?>">
                    หน้าจัดการโค้ด
                </a>
                
                <a class="nav-link" href="<?php echo e(url('/admin/wheel')); ?>">แก้ไขวงล้อ</a>
                <!-- <a class="nav-link" href="<?php echo e(url('/admin/userdata')); ?>">รายชื่อลูกค้าและที่อยู่</a> -->
                <a class="nav-link" href="<?php echo e(url('/admin/rewards')); ?>">ส่งของรางวัล</a>

            </nav>

        </div>
    </div>
</div>
<?php /**PATH /home/admin/domains/u12game-spin.com/public_html/resources/views/admin/sidebar.blade.php ENDPATH**/ ?>