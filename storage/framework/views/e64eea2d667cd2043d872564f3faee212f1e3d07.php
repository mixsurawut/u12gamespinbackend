<?php $__env->startSection('style'); ?>
<link rel="stylesheet" href="public/css/style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<style>
    html,body{
        font-family: 'Kanit', sans-serif !important;
    }
    .toast{
        background-color: #272727;
    }
</style>



    <div class="container">
        <?php if(!empty(Session::get('msg'))): ?>
            <?php echo Session::get('msg'); ?>

         <?php endif; ?>
        <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <ol class="carousel-indicators">
                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"></li>
                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"></li>

            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="public/media/banner1.jpg" class="d-block w-100">
                </div>
                <div class="carousel-item">
                    <img src="public/media/banner2.jpg" class="d-block w-100">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </a>
        </div>
    </div>

    <div id="container">


        <div class="wheelContainer">
            <svg class="wheelSVG" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" text-rendering="optimizeSpeed" preserveAspectRatio="xMidYMin meet">
                <defs>
                    <filter id="shadow" x="-100%" y="-100%" width="550%" height="550%">
                        <feOffset in="SourceAlpha" dx="0" dy="0" result="offsetOut"></feOffset>
                        <feGaussianBlur stdDeviation="9" in="offsetOut" result="drop" />
                        <feColorMatrix in="drop" result="color-out" type="matrix" values="0 0 0 0   0
              0 0 0 0   0
              0 0 0 0   0
              0 0 0 .3 0" />
                        <feBlend in="SourceGraphic" in2="color-out" mode="normal" />
                    </filter>
                </defs>
                <g class="mainContainer">
                    <g class="wheel">
                    </g>
                </g>
                <g class="centerCircle" />
                <g class="wheelOutline" />
                <g class="pegContainer" opacity="1">
                    <path class="peg" fill="#3b3b3b" d="M22.139,0C5.623,0-1.523,15.572,0.269,27.037c3.392,21.707,21.87,42.232,21.87,42.232 s18.478-20.525,21.87-42.232C45.801,15.572,38.623,0,22.139,0z" />
                </g>
                <g class="valueContainer" />
                <g class="centerCircleImageContainer" />
            </svg>
            <div class="toast">
                <p></p>
            </div>
        </div>

        <div class="token">
            <i class="fas fa-certificate"></i> x <?php echo e(isset($user->redeems)? count($user->redeems) : '-'); ?>


        </div>


        <button class="spinBtn">หมุนของรางวัล</button>

        <div class="container">

        <div class="redeem">
            <form action="/redeems" method="post">
                <?php echo e(csrf_field()); ?>

                <input type="text" name="name" class="form-control mt-4" placeholder="กรอกโค้ดที่นี่" aria-label="#code-here" aria-describedby="button-addon2">
                <button class="btn btn-outline-primary mt-2" type="submit" id="button-addon2">ใช้โค้ด</button>

            </form>
        </div>


        </div>

        <div class="container">

        <div class="card mt-5 shadow">
            <div class="card-header">
                วิธีร่วมกิจกรรม
            </div>
            <div class="card-body">
                <blockquote class="blockquote mb-0">
                    <ul >
                        <li >กิจกรรมนี้พิเศษเฉพาะสมาชิก U12 Game ทุกคน</li>
                        <li >ใช้ 100 คะแนน เพื่อรับสิทธิ์หมุนวงล้อร่วมสนุก 1 ครั้ง สมาชิกสามารถร่วมสนุกกี่ครั้งก็ได้ ไม่จำกัด</li>
                        <li >Step 1:สมาชิก Log in เข้าสู่ระบบ สำหรับคนที่ยังไม่เคยกรอกข้อมูลที่อยู่ในการจัดส่ง
                        จะต้องกรอกข้อมูลที่อยู่ให้เรียบร้อยก่อนที่หน้า ประวัติของคุณ</li>
                        <li >Step 2:ระบบจะแสดงของรางวัลที่คุณได้รับ สามารถตรวจสอบของรางวัลได้ที่หน้า รายการแลกของรางวัล</li>
                    </ul>

                </blockquote>
            </div>
        </div>

        <div class="card mt-4 shadow">
            <div class="card-header">
                เงื่อนไขการร่วมกิจกรรม
            </div>
            <div class="card-body">
                <blockquote class="blockquote mb-0">
                    <ul >
                        <li >กิจกรรม Lucky Wheel Lucky You หมุนปุ๊บ รับปั๊บ! สามารถร่วมกิจกรรมนี้ได้ตั้งแต่วันที่ 4 ก.ย. 2563 - 30 ก.ย. 2563
                        นี้เท่านั้น และจำกัดสิทธิ์ทั้งหมด 10,000 สิทธิ์ ตลอดระยะเวลากิจกรรม</li>
                        <li >ช่องทางร่วมกิจกรรม Lucky Wheel Lucky You หมุนปุ๊บ รับปั๊บ! ผ่านทางเว็บไซต์ https://u12game.com/</li>
                        <li >รางวัลที่ผู้ร่วมกิจกรรมได้รับจะแสดงอยู่ในหน้า "รายการเเลกของรางวัล"</li>
                        <li >รายละเอียดของรางวัลมีดังนี้
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">1</li>
                                <li class="list-group-item">2</li>
                                <li class="list-group-item">3</li>
                                <li class="list-group-item">4</li>
                                <li class="list-group-item">5</li>
                            </ul>
                        </li>

                        <li >รายละเอียดการจัดส่งของรางวัล
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">1</li>
                                <li class="list-group-item">2</li>
                                <li class="list-group-item">3</li>
                                <li class="list-group-item">4</li>
                                <li class="list-group-item">5</li>
                            </ul>
                        </li>
                        <li >ของรางวัลไม่สามารถแลกเปลี่ยนมูลค่าเป็นเงินสดได้ และไม่สามารถเปลี่ยนแปลงหรือโอนสิทธิ์ให้ผู้อื่นได้"</li>
                        <li >สำหรับผู้ที่ได้รับของรางวัล DYSON เครื่องหนีบผม Corrale™ รุ่น DY32358301 , สระน้ำเป่าลม Avenli ทรงกลม รุ่น 17797 และ
                        HAFELE เครื่องสกัดน้ำผลไม้ JUICE EXTRACTOR (ECOM-154) จะต้องชำระภาษี ณ ที่จ่าย 5%
                        ของมูลค่ารางวัลที่ได้รับหรือตามที่กฏหมายกำหนด เนื่องจากมีมูลค่าตั้งแต่ 1,000 บาท ขึ้นไป"</li>
                        <li >บริษัทของสงวนสิทธิ์การยกเลิกกิจกรรม Lucky Wheel Lucky You หมุนปุ๊บ รับปั๊บ! โดยมิต้องแจ้งให้ทราบล่วงหน้า</li>
                        <li >บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไขและของรางวัลโดยไม่ต้องแจ้งให้ทราบล่วงหน้า</li>
                        <li >ผู้ร่วมรายการถือว่ายอมรับกฎกติกา และข้อกำหนดสำหรับการเข้าร่วมรายการนี้เรียบร้อยแล้ว</li>
                        <li >ใบเสร็จหรือใบกำกับภาษีที่ร่วมรายการคือใบเสร็จหรือใบกำกับภาษีที่ซื้อผลิตภัณฑ์ Hi-Q UHT ได้แก่ Hi-Q1+ รสจืด, กลิ่นน้ำผึ้ง,
                        กลิ่นวานิลลา ทุกขนาด, Hi-Q3+ รสจืด, กลิ่นน้ำผึ้ง, กลิ่นวานิลลา, สูตรไขมันต่ำ ทุกขนาด, Dugro All n One รสจืด,
                        ยกเว้นนมดัดแปลงสำหรับทารกและ นมดัดแปลงสูตรต่อเนื่องสำหรับทารกและเด็กเล็กไม่ร่วมรายการกิจกรรมนี้</li>
                    </ul>

                </blockquote>
            </div>
        </div>

        </div>

        <footer>u12game.com</footer>


</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>

</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/utils/Draggable.min.js'></script>
<script src='public/js/ThrowPropsPlugin.min.js'></script>
<script src='public/js/Spin2WinWheel.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/TextPlugin.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" iegrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

<script src="public/js/index.js"></script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/admin/domains/u12game-spin.com/public_html/resources/views/home.blade.php ENDPATH**/ ?>