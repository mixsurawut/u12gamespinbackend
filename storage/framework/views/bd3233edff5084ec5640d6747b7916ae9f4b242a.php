<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <?php echo $__env->make('admin.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">ของรางวัล <?php echo e($item->full_name); ?></div>
                    <div class="card-body">

                        <a href="<?php echo e(url('/admin/rewards')); ?>" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> กลับ</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td><?php echo e($item->id); ?></td>
                                    </tr>
                                    <tr><th> ชื่อ - นามสกุล </th><td> <?php echo e($item->full_name); ?> (<?php echo e($item->phone); ?>)</td></tr><tr><th> รางวัลที่ได้ </th><td> <?php echo e($item->wheel->item_name); ?> </td></tr><tr><th> ที่อยู่</th><td> <?php echo e($item->address); ?> <?php echo e($item->post_code); ?> </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <?php if(!$item->status): ?>
                    <form method="POST" action="<?php echo e(url('/admin/rewards/' . $item->id)); ?>" accept-charset="UTF-8" class="form-horizontal"
                        enctype="multipart/form-data">
                        <?php echo e(method_field('PATCH')); ?>

                        <?php echo e(csrf_field()); ?>


                        <button class="btn btn-success w-100">ส่งสินค้า</button>

                    </form>
                    <?php else: ?>

                    ของรางวัลถูกจัดส่งแล้ว


                        <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/admin/domains/u12game-spin.com/public_html/resources/views/admin/reward/show.blade.php ENDPATH**/ ?>