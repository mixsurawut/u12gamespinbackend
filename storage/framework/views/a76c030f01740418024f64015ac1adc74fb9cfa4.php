<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
       <div class="col-md-8"> 
             <div class="card">
                <<div class="card-header text-center">



                </div>  
     <?php echo e(__('Login')); ?> 
                 <div class="card-body">
                    <form method="POST" action="<?php echo e(route('login')); ?>">
                        <?php echo csrf_field(); ?> 
 
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right"><?php echo e(__('E-Mail Address')); ?></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                    name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus>

                                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Password')); ?></label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password"
                                    required autocomplete="current-password">

                                <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div> 

                        <hr />
                        <p class="text-center" style="margin:20px;">หรือเข้าสู่ระบบด้วย</p>
       <div class="card mb-3">
                            <img class="card-img-top" src="/storage/app/public/uploads/Logo.jpg" alt=" Card image cap"
                                width="100px" height="100px">
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <a href="<?php echo e(url('/')); ?>/login/facebook" class="btn btn-primary btn-facebook">
                                            <i class="fa fa-facebook"></i> &nbsp; Login with Facebook
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


      <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>

                                    <label class="form-check-label" for="remember">
                                        <?php echo e(__('Remember Me')); ?>

                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    <?php echo e(__('Login')); ?>

                                </button>

                                <?php if(Route::has('password.request')): ?>
                                <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
                                    <?php echo e(__('Forgot Your Password?')); ?>

                                </a>
                                <?php endif; ?>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div> 
        <div class="form-group text-center">
            <img src="public/media/banner1.jpg" width="1024px">
            <br></br>
            <div class="col-md-12 text-center">
                <a href="<?php echo e(url('/')); ?>/login/facebook" class="btn btn-primary btn-facebook">
                    <i class="fa fa-facebook"></i> &nbsp; Login with Facebook
                </a>
            </div>
        </div>
    </div>
        </div> 
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\u12gamespinbackend\resources\views/auth/login.blade.php ENDPATH**/ ?>