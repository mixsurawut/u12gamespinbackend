<div class="form-group <?php echo e($errors->has('probability') ? 'has-error' : ''); ?>">
    <label for="probability" class="control-label"><?php echo e('Probability'); ?></label>
    <input class="form-control" name="probability" type="number" id="probability" value="<?php echo e(isset($wheel->probability) ? $wheel->probability : ''); ?>" required>
    <?php echo $errors->first('probability', '<p class="help-block">:message</p>'); ?>

</div>
<div class="form-group <?php echo e($errors->has('value') ? 'has-error' : ''); ?>">
    <label for="value" class="control-label"><?php echo e('Value'); ?></label>
    <input class="form-control" name="value" type="text" id="value" value="<?php echo e(isset($wheel->value) ? $wheel->value : ''); ?>" required>
    <?php echo $errors->first('value', '<p class="help-block">:message</p>'); ?>

</div>
<div class="form-group <?php echo e($errors->has('win') ? 'has-error' : ''); ?>">
    <label for="win" class="control-label"><?php echo e('Win'); ?></label>
    <div class="radio">
    <label><input name="win" type="radio" value="1" <?php echo e((isset($wheel) && 1 == $wheel->win) ? 'checked' : ''); ?>> Yes</label>
</div>
<div class="radio">
    <label><input name="win" type="radio" value="0" <?php if(isset($wheel)): ?> <?php echo e((0 == $wheel->win) ? 'checked' : ''); ?> <?php else: ?> <?php echo e('checked'); ?> <?php endif; ?>> No</label>
</div>
    <?php echo $errors->first('win', '<p class="help-block">:message</p>'); ?>

</div>
<div class="form-group <?php echo e($errors->has('result_text') ? 'has-error' : ''); ?>">
    <label for="result_text" class="control-label"><?php echo e('Result Text'); ?></label>
    <input class="form-control" name="result_text" type="text" id="result_text" value="<?php echo e(isset($wheel->result_text) ? $wheel->result_text : ''); ?>" required>
    <?php echo $errors->first('result_text', '<p class="help-block">:message</p>'); ?>

</div>
<div class="form-group <?php echo e($errors->has('item_name') ? 'has-error' : ''); ?>">
    <label for="item_name" class="control-label"><?php echo e('Item Name'); ?></label>
    <input class="form-control" name="item_name" type="text" id="item_name" value="<?php echo e(isset($wheel->item_name) ? $wheel->item_name : ''); ?>" required>
    <?php echo $errors->first('item_name', '<p class="help-block">:message</p>'); ?>

</div>
<div class="form-group <?php echo e($errors->has('item_img') ? 'has-error' : ''); ?>">
    <label for="item_img" class="control-label"><?php echo e('Item Img'); ?></label>
    <input class="form-control" name="item_img" type="file" id="item_img" value="<?php echo e(isset($wheel->item_img) ? $wheel->item_img : ''); ?>" required>
    <?php echo $errors->first('item_img', '<p class="help-block">:message</p>'); ?>

</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? 'Update' : 'Create'); ?>">
</div>
<?php /**PATH C:\xampp\htdocs\u12gamespinbackend\resources\views/admin/wheel/form.blade.php ENDPATH**/ ?>