<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">ประวัติการรับของรางวัล</div>
                    <div class="card-body">

                        <form action="">
                            <select name="status" onchange="this.form.submit()">
                                <option value="" <?php echo e($status == 'all' ? 'selected': ''); ?>>ทั้งหมด</option>
                                <option value="wait" <?php echo e($status == 'wait' ? 'selected': ''); ?>>รอจัดส่ง</option>
                                <option value="success"  <?php echo e($status == 'success' ? 'selected': ''); ?>>สำเร็จ</option>
                        </select>

                        </form>




                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>ชื่อ - นามสกุล</th><th>ของรางวัลที่ได้</th><th>เวลาที่ได้</th><th>สถานะ</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($loop->iteration); ?></td>
                                        <td><?php echo e($item->full_name); ?></td><td><?php echo e($item->wheel->item_name); ?></td><td><?php echo e($item->created_at); ?></td>
                                        <td>
                                            <span class="badge badge-<?php echo e($item->status ? 'warning' : 'success'); ?>">
                                            <?php echo e($item->status ? 'ได้รับแล้ว': 'รอจัดส่ง'); ?>

                                            </span>
                                        </td>
                                        <td>
                                            <a href="<?php echo e(url('/my_rewards/' . $item->id)); ?>" title="View Wheel"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo e($item->status  ? 'ดูข้อมูล': 'ดูข้อมูลและส่งรางวัล'); ?></button></a>
                                            <!-- <a href="<?php echo e(url('/admin/rewards/' . $item->id . '/edit')); ?>" title="Edit Wheel"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a> -->


                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo $items->appends(['search' => Request::get('search')])->render(); ?> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/admin/domains/u12game-spin.com/public_html/resources/views/myreward/index.blade.php ENDPATH**/ ?>