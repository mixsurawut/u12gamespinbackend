<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('จำนวนโค้ดที่ต้องการ'); ?></label>
    <input class="form-control" name="num" type="text" id="name" value="<?php echo e(isset($code->name) ? $code->name : ''); ?>" required>
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>

<div class="form-group <?php echo e($errors->has('credit') ? 'has-error' : ''); ?>">
    <label for="credit" class="control-label"><?php echo e('จำนวนเครดิตที่ต้องการ'); ?></label>
    <input class="form-control" name="credit" type="text" id="credit" value="<?php echo e(isset($code->credit) ? $code->credit : ''); ?>" required>
    <?php echo $errors->first('credit', '<p class="help-block">:message</p>'); ?>

</div>



<div class="form-group">
    <input class="btn btn-primary" type="submit" value="<?php echo e($formMode === 'edit' ? 'Update' : 'สุ่ม'); ?>">
</div>
<?php /**PATH /home/admin/domains/u12game-spin.com/public_html/resources/views/admin/codes/form.blade.php ENDPATH**/ ?>