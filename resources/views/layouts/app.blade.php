<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('public/js/app.js') }}" defer></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="public/media/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="public/media/favicon//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="public/media/favicon//favicon-16x16.png">
    <link rel="manifest" href="public/media/favicon//site.webmanifest">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">


    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">

        html,body{
            font-family: 'Kanit', sans-serif !important;
            background-color: #0a0a0a !important;
            background-color: rgb(0,0,0);
            background-color: -moz-linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(25,137,226,1) 100%);
            background-color: -webkit-linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(25,137,226,1) 100%);
            background-color: linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(25,137,226,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#000000",endColorstr="#1989e2",GradientType=1);
            background-repeat: no-repeat;
            background-image: url('public/media/bg-main.jpg?v') !important;
            background-position: center  !important;
            background-position: top  !important;
        }
        @media only screen and (max-width: 1440px){
            body{
                background-image: url('public/media/bg-main-1440.jpg') !important;
                background-attachment: unset !important;
                background-repeat: no-repeat !important;
                background-position: unset !important;
                background-size: 100% auto;
                background: #0e0e0e;
            }
        }
        @media only screen and (max-width: 768px){
            body{
                background-image: url('public/media/bg-content-mb-3.jpg') !important;
                background-attachment: unset !important;
                background-repeat: no-repeat !important;
                background-position: unset !important;
                background: #0e0e0e;
            }
        }
        .btn-facebook {
            color: white !important;
            background-color: #4267b2 !important;
            border-bottom: 1px solid #4267b2 !important;
        }
        .navbar-toggler{
            width: 100% !important;
            box-shadow: none;
        }
        .bg-light{
            background-color: #fff !important;
        }
    </style>
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    @yield('style')
</head>

<body>

    <div id="app">

        <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src="https://u12game.com/wp-content/uploads/2021/02/Screenshot_5-1080x123-1-1024x117.jpg" alt="" width="100%">

                        </a>
                    </div>
                    <div class="col-sm-6">

                            <ul class="navbar-nav ml-auto text-center">
                                    <!-- Authentication Links -->
                                    @guest
                                    <li class="nav-item m-auto text-center">
                                        <p class="mt-2">กรุณาเข้าสู่ระบบ</p>
                                    </li>
                                    <!-- <li class="nav-item my-auto">
                                        <a class="nav-link" href="{{ route('login') }}"></a>
                                         {{ __('Login') }}
                                    </li> -->
                                    @if (Route::has('register'))
                                    <!-- <li class="nav-item my-auto">
                                        <a class="nav-link" href="{{ route('register') }}"></a>
                                        {{ __('Register') }}
                                    </li> -->
                                    @endif
                                    @else


                                    <ul class="navbar-nav nav justify-content-end mt-3">
                                        <!-- <li class="nav-item my-auto">
                                            <p>
                                                {{ Auth::user()->name }}
                                            </p>
                                        </li> -->
                                        <!-- <li class="nav-item my-auto">
                                            <a class="dropdown-item" href="{{ url('/admin/codes') }}">
                                                Dashboard

                                            </a>
                                        </li> -->
                                        <li class="nav-item my-auto mx-auto">
                                            @if(Auth::user()->role == 2||Auth::user()->role == 3)
                                            <a class="nav-link mt-2" href="{{ url('/admin/codes') }}">
                                                แดชบอร์ด
                                            </a>
                                            @endif

                                        </li>
                                        <li class="nav-item my-auto mx-auto">
                                        <a class="nav-link mt-2" href="{{ url('/my_rewards') }}">
                                                ประวัติการหมุน
                                            </a>
                                        </li>

                                        <li class="nav-item my-auto mx-auto">
                                            <a class="nav-link mt-2" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                                                                     document.getElementById('logout-form').submit();">
                                                {{ __('ออกจากระบบ') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                    <!--
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        </div> -->

                                    @endguest
                                </ul>

                    </div>
                </div>


            </div>
        </nav>

        <main class="">
            @yield('content')
        </main>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"
            integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ=="
            crossorigin="anonymous"></script>
        @yield('script')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/utils/Draggable.min.js'></script>
    <script src='public/js/ThrowPropsPlugin.min.js'></script>
    <script src='public/js/Spin2WinWheel.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/TextPlugin.min.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" iegrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

    <script src="public/js/index.js"></script>


    </div>
</body>

</html>

<style>
.navbar-light .navbar-toggler {
    color: rgba(0,0,0,.55);
    border-color: rgba(0,0,0,.1);
    width: 55px;
}
</style>
