@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="public/css/style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
@endsection


@section('content')
<style>
    html {
        height: 100%;
    }
    html,body{
            background-image: url('public/media/bg1.jpg');
            font-family: 'Kanit', sans-serif !important;
            background-color: #0f0f0f !important;
            background-color: rgb(0,0,0);
            background-color: -moz-linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(25,137,226,1) 100%);
            background-color: -webkit-linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(25,137,226,1) 100%);
            background-color: linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(25,137,226,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#000000",endColorstr="#1989e2",GradientType=1);
            background-attachment: unset;
            background-repeat: no-repeat;
    }
    .wheel *{
        font-family: 'Kanit', sans-serif !important;
    }
    .toast{
        background-color: #272727 !important;
    }
    .container-banner{
        width: 100%;
        max-width: 1920px;
        margin-top: -24px
    }
    .card-wheel{
        width: 100%;
    }
    .token{
        color: #fff;
        margin-bottom: 15px;
    }
    .spinBtn{
        background-color: #004DA0;
        border: 4px solid #568FFD;
        border-radius: 10px;
    }
    .redeem button {
        border: 2px solid #FFF;
        color: #FFF;
        width: 100%;
    }
    .redeem input {
        width: 100%;
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .spin-form button{
        width: 100%;
    }
    .promotion-content{
        display: none;
        text-align: center;
    }
    @media only screen and (max-width: 768px){
        .promotion-content{
        display: block;
        text-align: center;
    }
    .promotion-content img{
        width: 100%;
        text-align: center;
        max-width: 320px;
    }
    }
    @media only screen and (max-width: 480px){
    .wheelSVG {
        position: relative;
        overflow: visible;
        height: 44vh;
    }
    }
    .navbar-light .navbar-nav .nav-link {
        color: rgba(0,0,0,.55);
        margin-top: -10px;
    }

    ul,
    li {
        margin-top: 10px;
    }

    footer {
        text-align: center;
        font-weight: 200;
        color: rgb(255, 255, 255);
        background-color: #031a36;
        padding: 20px;
        margin-top: 35px;
    }
    .toast p, .toast span ,.mainContainer,.valueContainer{
        font-family: 'Kanit', sans-serif !important;
    }
</style>



    <!-- <div class="container-banner m-auto text-center">

        @if(!empty(Session::get('msg')))
            {!! Session::get('msg') !!}
         @endif
        <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <ol class="carousel-indicators">
                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"></li>
                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"></li>
                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="https://i.ibb.co/Phwhjr1/banner1.jpg" class="d-block w-100 img-fluid">
                </div>
                <div class="carousel-item">
                    <img src="https://i.ibb.co/DfdSPys/banner2.jpg" class="d-block w-100 img-fluid">
                </div>
                <div class="carousel-item">
                    <img src="https://i.ibb.co/1m0y6mb/banner3.jpg" class="d-block w-100 img-fluid">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </a>
        </div>

    </div> -->

    <div class="promotion-content mt-3 mx-auto ">
        <img src="public/media/mb-x2t.png" alt="">
    </div>

    <div id="container">
        <div class="card-wheel">
        <div class="wheelContainer">
            <svg class="wheelSVG" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" text-rendering="optimizeSpeed" preserveAspectRatio="xMidYMin meet">
                <defs>
                    <filter id="shadow" x="-100%" y="-100%" width="550%" height="550%">
                        <feOffset in="SourceAlpha" dx="0" dy="0" result="offsetOut"></feOffset>
                        <feGaussianBlur stdDeviation="9" in="offsetOut" result="drop" />
                        <feColorMatrix in="drop" result="color-out" type="matrix" values="0 0 0 0   0
              0 0 0 0   0
              0 0 0 0   0
              0 0 0 .3 0" />
                        <feBlend in="SourceGraphic" in2="color-out" mode="normal" />
                    </filter>
                </defs>
                <g class="mainContainer">
                    <g class="wheel">
                    </g>
                </g>
                <g class="centerCircle" />
                <g class="wheelOutline" />
                <g class="pegContainer" opacity="1">
                    <path class="peg" fill="#3b3b3b" d="M22.139,0C5.623,0-1.523,15.572,0.269,27.037c3.392,21.707,21.87,42.232,21.87,42.232 s18.478-20.525,21.87-42.232C45.801,15.572,38.623,0,22.139,0z" />
                </g>
                <g class="valueContainer" />
                <g class="centerCircleImageContainer" />
            </svg>
            <div class="toast">
                <p></p>
            </div>
        </div>

        </div>
    </div>

    <div class="container mt-3">
        <div class="row">
            <div class="col col-sm-12 col-lg-4 shadow-sm m-auto">
                <div class="spin-form">
                    <div class="token">
                        <i class="fas fa-certificate"></i> x {{isset($user->redeems)? count($user->redeems) : '-'}}
                    </div>

                    <button class="spinBtn shadow">หมุนของรางวัล</button>

                    <div class="redeem">
                        <form action="/redeems" method="post">
                            {{csrf_field()}}
                            <input type="text" name="name" class="form-control mt-4" placeholder="กรอกโค้ดที่นี่" aria-label="#code-here" aria-describedby="button-addon2">
                            <button class="btn btn-outline-primary mt-2" type="submit" id="button-addon2">ใช้โค้ด</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="promotion-content mt-5 mx-auto">
        <img src="public/media/mb-jackpot.png" alt="">
    </div>


        <div class="container">

            <div class="row">
                <div class="col col-sm-12 col-12 shadow-sm m-auto">
                    <div class="card mt-5 shadow">
                        <div class="card-header">
                            วิธีร่วมกิจกรรม
                        </div>
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <ul >
                                    <li >กิจกรรมพิเศษ เฉพาะสมาชิก U12Game </li>
                                     <li >เฉพาะเติมเงิน โซนกีฬาเท่านั้น </li>
                                    <li >ทุกๆ 50 บาท รับสิทธิ์หมุนวงล้อร่วมสนุก 1 ครั้ง สมาชิกสามารถร่วมสนุกกี่ครั้งก็ได้ ไม่จำกัด ตามจำนวนเงินที่เติม</li>
                                </ul>

                            </blockquote>
                        </div>
                    </div>

                    <div class="card mt-4 shadow">
                        <div class="card-header">
                            วิธีการเล่น Lukcy Spin
                        </div>
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <ul >
                                    <li >เติมเงินเพื่อรับสิทธิพิเศษมากมาย</li>
                                    <li >รับโค้ดจากทาง U12GAME</li>
                                    <li >นำโค้ดที่ได้นำไปกรอกในช่อง กรอกโค้ด</li>
                                    <li >หลังจากนั้นให้กดปุ่ม “ใช้โค้ด”</li>
                                    <li >หมุนวงล้อเพื่อสุ่มของรางวัล</li>
                                    <li >หลังจากสุ่มเสร็จจะพาไปยังหน้า รับรางวัล</li>
                                    <li >สามารถกดปุ่ม ”กลับไปหมุนสปิน” เพื่อกลับมาสุ่มอีกครั้ง</li>
                                    <li >สามารถดูประวัติการสุ่มได้ในหน้า “ประวัติการหมุน”</li>
                                    <li >เมื่อทำการเล่นเสร็จสิ้นแล้ว ทางทีมงานจะยืนยันการส่งของ ตามที่อยู่ที่ท่านกรอกมา</li>
                                    <li >ท่านสามารถตรวจสอบสถานะการส่งของได้ที่หน้า "ประวัติการหมุน" ที่เมนูด้านบนได้ตลอดเวลา</li>
                                    <li >รายละเอียดของรางวัลมีดังนี้
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">1. ของรางวัลใหญ่ ประจำสัปดาห์ </li>
                                            <li class="list-group-item">2. เครดิตฟรี</li>
                                            <li class="list-group-item">3. รางวัลอื่นๆ</li>
                                        </ul>
                                    </li>
                                </ul>

                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <footer>u12game.com</footer>


        </div>

@endsection

@section('script')
<script>

</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/utils/Draggable.min.js'></script>
<script src='public/js/ThrowPropsPlugin.min.js'></script>
<script src='public/js/Spin2WinWheel.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/TextPlugin.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" iegrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

<script src="js/index.js"></script>


@endsection
