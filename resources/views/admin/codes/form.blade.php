{{-- <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'จำนวนโค้ดที่ต้องการ' }}</label>
    <input class="form-control" name="num" type="text" id="name" value="{{ isset($code->name) ? $code->name : ''}}" required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div> --}}

<div class="form-group {{ $errors->has('cash') ? 'has-error' : ''}}">
    <label for="cash" class="control-label">{{ 'จำนวนเงินที่เติม' }}</label>
    <input class="form-control" name="cash" type="text" id="cash" value="{{ isset($code->cash) ? $code->cash : ''}}" required>
    {!! $errors->first('cash', '<p class="help-block">:message</p>') !!}
</div>



<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'สุ่ม' }}">
</div>
