@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">ส่งของรางวัล</div>
                    <div class="card-body">

                        <form action="">
                            <select name="status" onchange="this.form.submit()">
                                <option value="" {{$status == 'all' ? 'selected': ''}}>ทั้งหมด</option>
                                <option value="wait" {{$status == 'wait' ? 'selected': ''}}>รอ</option>
                                <option value="success"  {{$status == 'success' ? 'selected': ''}}>สำเร็จ</option>
                        </select>

                        </form>


                        <form method="GET" action="{{ url('/admin/rewards') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>ชื่อ - นามสกุล</th><th>ของรางวัลที่ได้</th><th>เวลาที่ได้</th><th>สถานะ</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->full_name }}</td><td>{{ $item->wheel->item_name }}</td><td>{{ $item->created_at }}</td>
                                        <td>
                                            <span class="badge badge-{{$item->status ? 'warning' : 'success'}}">
                                            {{$item->status ? 'ส่งแล้ว': 'ยังไม่ได้ส่ง'}}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/rewards/' . $item->id) }}" title="View Wheel"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> {{$item->status  ? 'ดูข้อมูล': 'ดูข้อมูลและส่งรางวัล'}}</button></a>
                                            <!-- <a href="{{ url('/admin/rewards/' . $item->id . '/edit') }}" title="Edit Wheel"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a> -->


                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $items->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
