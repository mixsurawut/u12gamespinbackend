@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">ของรางวัล {{ $item->full_name }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/rewards') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> กลับ</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $item->id }}</td>
                                    </tr>
                                    <tr><th> Username </th><td>{{$item->post_code}} </td></tr>
                                    <tr><th> ชื่อ - นามสกุล </th><td> {{ $item->full_name }} ({{$item->phone}})</td></tr>
                                    <tr><th> รางวัลที่ได้ </th><td> {{ $item->wheel->item_name }} </td></tr>
                                    <tr><th> ที่อยู่</th><td> {{ $item->address }} {{$item->post_code}} </td></tr>

                                </tbody>
                            </table>
                        </div>

                    </div>

                    @if(!$item->status)
                    <form method="POST" action="{{ url('/admin/rewards/' . $item->id) }}" accept-charset="UTF-8" class="form-horizontal"
                        enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <button class="btn btn-success w-100">ส่งสินค้า</button>

                    </form>
                    @else

                    <p class="text-center py-3 btn-success" >ของรางวัลถูกจัดส่งแล้ว</p>


                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
