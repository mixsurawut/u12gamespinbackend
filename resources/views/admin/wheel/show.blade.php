@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">ข้อมูลของรางวัล {{ $wheel->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/wheel') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> กลับ</button></a>
                        <a href="{{ url('/admin/wheel/' . $wheel->id . '/edit') }}" title="Edit Wheel"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไข</button></a>

                        <form method="POST" action="{{ url('admin/wheel' . '/' . $wheel->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Wheel" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> ลบ</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $wheel->id }}</td>
                                    </tr>
                                    <tr><th> Probability </th><td> {{ $wheel->probability }} </td></tr><tr><th> Value </th><td> {{ $wheel->value }} </td></tr><tr><th> Win </th><td> {{ $wheel->win }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
