<div class="form-group {{ $errors->has('probability') ? 'has-error' : ''}}">
    <label for="probability" class="control-label">{{ 'ความเป็นไปได้ (0-100)' }}</label>
    <input class="form-control" name="probability" type="number" id="probability" value="{{ isset($wheel->probability) ? $wheel->probability : ''}}" required>
    {!! $errors->first('probability', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('value') ? 'has-error' : ''}}">
    <label for="value" class="control-label">{{ 'ชื่อของรางวัล' }}</label>
    <input class="form-control" name="value" type="text" id="value" value="{{ isset($wheel->value) ? $wheel->value : ''}}" required>
    {!! $errors->first('value', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('win') ? 'has-error' : ''}}">
    <label for="win" class="control-label">{{ 'Win' }}</label>
    <div class="radio">
    <label><input name="win" type="radio" value="1" {{ (isset($wheel) && 1 == $wheel->win) ? 'checked' : '' }}> Yes</label>
</div>
<div class="radio">
    <label><input name="win" type="radio" value="0" @if (isset($wheel)) {{ (0 == $wheel->win) ? 'checked' : '' }} @else {{ 'checked' }} @endif> No</label>
</div>
    {!! $errors->first('win', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('result_text') ? 'has-error' : ''}}">
    <label for="result_text" class="control-label">{{ 'คำตอนได้ของรางวัล' }}</label>
    <input class="form-control" name="result_text" type="text" id="result_text" value="{{ isset($wheel->result_text) ? $wheel->result_text : ''}}" required>
    {!! $errors->first('result_text', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('item_name') ? 'has-error' : ''}}">
    <label for="item_name" class="control-label">{{ 'ชื่อของรางวัล' }}</label>
    <input class="form-control" name="item_name" type="text" id="item_name" value="{{ isset($wheel->item_name) ? $wheel->item_name : ''}}" required>
    {!! $errors->first('item_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('item_img') ? 'has-error' : ''}}">
    <label for="item_img" class="control-label">{{ 'รูปภาพของรางวัล' }}</label>
    <input class="form-control" name="item_img" type="file" id="item_img" value="{{ isset($wheel->item_img) ? $wheel->item_img : ''}}" required>
    {!! $errors->first('item_img', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
