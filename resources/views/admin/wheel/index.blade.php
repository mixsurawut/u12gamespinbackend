@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">วงล้อ</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/wheel/create') }}" class="btn btn-success btn-sm" title="Add New Wheel">
                            <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มของรางวัล
                        </a>

                        <form method="GET" action="{{ url('/admin/wheel') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="ค้นหา..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Probability</th><th>Value</th><th>Win</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($wheel as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->probability }}</td><td>{{ $item->value }}</td><td>{{ $item->win }}</td>
                                        <td>
                                            <a href="{{ url('/admin/wheel/' . $item->id) }}" title="View Wheel"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> ดู</button></a>
                                            <a href="{{ url('/admin/wheel/' . $item->id . '/edit') }}" title="Edit Wheel"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไข</button></a>

                                            <form method="POST" action="{{ url('/admin/wheel' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Wheel" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> ลบ</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $wheel->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
