@extends('layouts.app')

@section('content')
    <div class="container m-auto text-center">
        <div class="mt-5 mb-5">
        <div class="row">

            <div class="col-md-12">
                <div class="m-auto">
                <div class="card">

                        <a href="/">
                            <button class="btn btn-primary float-left ml-3 mt-3">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> กลับไปหมุนสปินต่อ
                            </button>
                        </a>

                    <div class="card-header"><h3>ประวัติการรับของรางวัล</h3></div>
                    <div class="card-body">

                        <form action="">
                            <select name="status" onchange="this.form.submit()">
                                <option value="" {{$status == 'all' ? 'selected': ''}}>ทั้งหมด</option>
                                <option value="wait" {{$status == 'wait' ? 'selected': ''}}>รอจัดส่ง</option>
                                <option value="success"  {{$status == 'success' ? 'selected': ''}}>สำเร็จ</option>
                        </select>

                        </form>




                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>ชื่อ - นามสกุล</th><th>ของรางวัลที่ได้</th><th>เวลาที่ได้</th><th>สถานะ</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->full_name }}</td><td>{{ $item->wheel->item_name }}</td><td>{{ $item->created_at }}</td>
                                        <td>
                                            <span class="badge badge-{{$item->status ? 'warning' : 'success'}}">
                                            {{$item->status ? 'ได้รับแล้ว': 'รอจัดส่ง'}}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{ url('/my_rewards/' . $item->id) }}" title="View Wheel"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> {{$item->status  ? 'ดูข้อมูล': 'ดูข้อมูลและส่งรางวัล'}}</button></a>
                                            <!-- <a href="{{ url('/admin/rewards/' . $item->id . '/edit') }}" title="Edit Wheel"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a> -->


                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $items->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                        <a href="/">
                            <button class="btn btn-primary float-left mt-2">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> กลับไปหมุนสปินต่อ
                            </button>
                        </a>

                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection
