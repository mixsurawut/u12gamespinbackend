@extends('layouts.app')

@section('content')

    <div class="container">
        <!-- <div class="row justify-content-center">
           <div class="col-md-8">
                <div class="card">
                     <div class="card-header text-center">



                    </div>
                    {{ __('Login') }}
                  <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address')
                                    }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password')
                                    }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <hr />
                            <p class="text-center" style="margin:20px;">หรือเข้าสู่ระบบด้วย</p>
           <div class="card mb-3">
                                <img class="card-img-top" src="/storage/app/public/uploads/Logo.jpg" alt=" Card image cap"
                                    width="100px" height="100px">
                                <div class="card-body">
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <a href="{{url('/')}}/login/facebook" class="btn btn-primary btn-facebook">
                                                <i class="fa fa-facebook"></i> &nbsp; Login with Facebook
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>


           <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{
                                            old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div> -->
            <!-- <img src="public/media/banner3.jpg" width="100%" style="max-width: 1920px;margin: 0 auto;" class="">
            <div class="form-group text-center"> -->

                <!-- <div class="login-img-logo rounded-circle mt-5">
                    <img src="public/media/logo.png" alt="" class="text-center  mt-5" width="150px">
                </div> -->
                <div class="promotion-content mt-3 ">
                    <img src="public/media/mb-x2t.png" alt="">
                </div>
                <div class="wheel-login mt-3 text-center mb-2">
                    <img src="public/media/wheel.png" alt="">
                </div>
                <div class="col-md-12 text-center login-btn mt-5 mb-2 mx-auto">
                    <p class="sign-in-fb-text">เข้าสู่ระบบด้วย Facebook</p>
                    <a href="{{url('/')}}/login/facebook" class="btn btn-primary btn-facebook shadow">
                        <i class="fa fa-facebook"></i> &nbsp; Sign in with Facebook
                    </a>
                </div>
                <div class="promotion-content mt-5">
                    <img src="public/media/mb-jackpot.png" alt="">
                </div>
            </div>
    </div>



<style>
    p,a{
        font-family: 'Kanit', sans-serif !important;
    }
    .promotion-content{
        display: none;
        text-align: center;
    }
    .btn-facebook{
        margin-top: -10px;
        padding: 10px;
        font-size: 1.25rem;
        background-color: #4e6cac !important;
        color: rgb(255, 255, 255) !important;
        border: none;
        font-weight: 400;
}
    .sign-in-fb-text{
        color: #fff;
        text-align: center;
        padding-bottom: -10px;
        font-size: 1.35rem;
    }
    .btn-facebook i{
            padding: 10px 14px 10px 14px ;
            font-size: 1.25rem;
            background-color: #fff !important;
            color: #4e6cac !important;
            border: none;
            border-radius: 5px;
            font-weight: 600;
    }
    .login-img-logo{
        width: 250px;
        height: 250px;
        padding: 10px;
        background-color: #fff;
        border-radius: 100%;
        margin: 0 auto;
        text-align: center;
        background-position: center;
    }
    .login-btn{
        max-width: 300px;
        margin-top: -100px !important;
        border-radius: 10px;
        border:2px solid #fff;
        padding: 10px;
    }
    .bg-login{
        position: absolute;
        background-image: url('public/media/bg1.jpg');
        width: 100%;
        height: 100%;
        background-repeat: no-repeat;
        background-position: center;
    }
    @media only screen and (max-width: 768px){
        body{
        background-image: url('public/media/bg-content-mb.jpg');
    }
    .promotion-content{
        display: block;
        text-align: center;
    }
    .promotion-content img{
        width: 100%;
        max-width: 375px;
    }
    .wheel-login{
        display: block;
        text-align: center;
    }
    .wheel-login img{
        width: 100%;
        max-width: 375px;
    }
    .login-btn{
        margin-top: 30px !important;
    }
    }
    @media only screen and (max-width: 480px){
        .promotion-content img{
        width: 100%;
    }
    }
</style>
<!-- </div>
</div>
</div>  -->
@endsection
