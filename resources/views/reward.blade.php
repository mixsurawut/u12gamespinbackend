@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="css/style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet"
    crossorigin="anonymous">
<link
    href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap"
    rel="stylesheet">
@endsection


@section('content')
<style>
    html,
    body {
        font-family: 'Kanit', sans-serif !important;
    }
    .reward-box{
        margin-top: 50px;
        width: 100%;
    }
    .reward-title{
        text-align: center;
    }
    .reward-img img{
        max-width: 400px;
        width: 100%;
    }
    .reward-name{
        text-align: center;
        color: #fff;
        font-size: 1.75em;
    }
    .btn-box button{
        width: 300px;
        margin: 0 auto;
    }
    @media only screen and (max-width: 480px){
    .reward-img img{
        max-width: 250px;
        width: 100%;
    }
    }
</style>



<div class="container">

    <!-- Reward -->

<!-- <code>
    {{$reward->wheel}}
</code> -->

<div class="row">
    <div class="col col-12">
        <div class="reward-box">
            <div class="reward-title">
                <h2 class="mt-5 text-white mb-5">คุณได้รับรางวัล</h2>
            </div>
            <div class="reward-img text-center">
                <img src="{{ url('/storage/app/public/'. $reward->wheel->item_img) }}" alt="">

            </div>

            <br>

            <div class="reward-name">
                {{$reward->wheel->item_name}}
            </div>

        </div>

        <div class="btn-box text-center">
            <div class="row">
                    <div class="col col-12">
                        <a href="/">
                            <button class="btn btn-primary mt-4 p-3">
                                กลับไปหมุนสปินต่อ
                            </button>
                        </a>
                    </div>
                    <div class="col col-12">
                        <a href="/my_rewards">
                            <button class="btn btn-info mt-3 p-3">
                                ดูประวัติของรางวัลของฉัน
                            </button>
                            </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>




</div>

@endsection

@section('script')
<script>

</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/utils/Draggable.min.js'></script>
<script src='js/ThrowPropsPlugin.min.js'></script>
<script src='js/Spin2WinWheel.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/TextPlugin.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"
    integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"
    iegrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

<script src="js/index.js"></script>


@endsection
