<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWheelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wheels', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('probability')->nullable();
            $table->string('value')->nullable();
            $table->boolean('win')->nullable();
            $table->string('result_text')->nullable();
            $table->string('item_name')->nullable();
            $table->string('item_img')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wheels');
    }
}
