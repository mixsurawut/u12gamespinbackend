//Usage

//load your JSON (you could jQuery if you prefer)
function loadJSON(callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', '/wheel', true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            //Call the anonymous function (callback) passing in the response
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

//your own function to capture the spin results
function myResult(e) {
    var form = new FormData();
    form.append("wheel_id", e.userData.id);

    var settings = {
        "url": "rewards",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": JSON.stringify({
            wheel_id: e.userData.id
        })
    };

    $.ajax(settings).done(function (response) {

        let res = JSON.parse(response)

        if (res.key) {
            window.location.href = "/rewards/" + res.key

        } else {
            alert('ขณะนี้มีผู้ใช้บริการจำนวนมาก กรุณาลองใหม่อีกครั้ง\nหรือทำการ รีเฟรช,ปิด-เปิด ตัวเบราเซอร์ใหม่ค่ะ\nขออภัยในความไม่สะดวกค่ะ\n*บนเดสก์ท็อปกด Ctrl+Shift+R')
            location.reload()
        }

    });



}

//your own function to capture any errors
function myError(e) {
    //e is error object


    console.log('Spin Count: ' + e.spinCount + ' - ' + 'Message: ' + e.msg);

}

function myGameEnd(e) {


    //e is gameResultsArray
    console.log(e);
    //reset the wheel at the end of the game after 5 seconds
    /* TweenMax.delayedCall(5, function(){

      Spin2WinWheel.reset();

    })*/


}


function init() {

    loadJSON(function (response) {
        // Parse JSON string to an object
        var jsonData = JSON.parse(response);
        //if you want to spin it using your own button, then create a reference and pass it in as spinTrigger
        var mySpinBtn = document.querySelector('.spinBtn');
        //create a new instance of Spin2Win Wheel and pass in the vars object
        var myWheel = new Spin2WinWheel();

        //WITH your own button
        myWheel.init({ data: jsonData, onResult: myResult, onGameEnd: myGameEnd, onError: myError, spinTrigger: mySpinBtn });

        //WITHOUT your own button
        //myWheel.init({data:jsonData, onResult:myResult, onGameEnd:myGameEnd, onError:myError});
    });
}



//And finally call it
init();
