<?php

namespace App\Http\Middleware;

use Closure;

class NeedAddress
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $user = $request->user();

        if(!$user){
            return redirect('/login');
        }


      if(!$user->full_name && !$user->address && !$user->post_code && !$user->phone){
            return redirect('/address');
      }
        return $next($request);
    }
}
