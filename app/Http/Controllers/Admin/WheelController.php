<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Wheel;
use Illuminate\Http\Request;

class WheelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $wheel = Wheel::where('probability', 'LIKE', "%$keyword%")
                ->orWhere('value', 'LIKE', "%$keyword%")
                ->orWhere('win', 'LIKE', "%$keyword%")
                ->orWhere('result_text', 'LIKE', "%$keyword%")
                ->orWhere('item_name', 'LIKE', "%$keyword%")
                ->orWhere('item_img', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $wheel = Wheel::latest()->paginate($perPage);
        }

        return view('admin.wheel.index', compact('wheel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.wheel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'probability' => 'required',
			'value' => 'required',
			'result_text' => 'required',
			'item_name' => 'required',
			'item_img' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('item_img')) {
            $requestData['item_img'] = $request->file('item_img')
                ->store('uploads', 'public');
        }

        Wheel::create($requestData);

        return redirect('admin/wheel')->with('flash_message', 'Wheel added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $wheel = Wheel::findOrFail($id);

        return view('admin.wheel.show', compact('wheel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $wheel = Wheel::findOrFail($id);

        return view('admin.wheel.edit', compact('wheel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'probability' => 'required',
			'value' => 'required',
			'result_text' => 'required',
			'item_name' => 'required',
			'item_img' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('item_img')) {
            $requestData['item_img'] = $request->file('item_img')
                ->store('uploads', 'public');
        }

        $wheel = Wheel::findOrFail($id);
        $wheel->update($requestData);

        return redirect('admin/wheel')->with('flash_message', 'Wheel updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Wheel::destroy($id);

        return redirect('admin/wheel')->with('flash_message', 'Wheel deleted!');
    }
}
