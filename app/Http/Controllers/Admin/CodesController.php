<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Code;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $codes = Code::where('name', 'LIKE', "%$keyword%")
                ->latest()->whereDoesntHave('redeem')->paginate($perPage);
        } else {
            $codes = Code::latest()->whereDoesntHave('redeem')->paginate($perPage);
        }

        return view('admin.codes.index', compact('codes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.codes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        // for($i =0 ; $i<$request->num;$i++){
            $random = 'U12G'.Str::random(6).time();
            $credit = $request->cash/50;
            $credit = intval($credit);


            Code::create([
                'name'=> Str::upper($random),
                'credit' => $credit
            ]);
        // }


        return redirect('admin/codes')->with('flash_message', 'Code added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $code = Code::findOrFail($id);

        return view('admin.codes.show', compact('code'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $code = Code::findOrFail($id);

        return view('admin.codes.edit', compact('code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();

        $code = Code::findOrFail($id);
        $code->update($requestData);

        return redirect('admin/codes')->with('flash_message', 'Code updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Code::destroy($id);

        return redirect('admin/codes')->with('flash_message', 'Code deleted!');
    }
}
