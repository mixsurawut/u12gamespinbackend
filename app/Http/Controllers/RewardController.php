<?php

namespace App\Http\Controllers;

use App\Wheel;
use App\Redeem;
use App\Reward;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class RewardController extends Controller
{



    public function show($key, Request $request)
    {
        $reward = Reward::where('key', $key)->with('wheel')->first();

        return view('reward', compact('reward'));
    }

    public function store(Request $request)
    {

        $user = $request->user();
        $redeem = Redeem::where('user_id', $user->id)->whereNull('used_at')->first();

        if (!$redeem) {
            return ['status' => false, 'message' => 'ไม่สามารถใช้งานได้รีดีมหมด'];
        }

        $decrypted = Crypt::decryptString($request->wheel_id);

        $check = Reward::where('user_id', $user->id)->where('key', $request->wheel_id)->first();

        if ($check) {
            return ['status' => false, 'message' => 'มีการใช้งานแล้ว'];
        }

        $reward = Reward::create([
            'user_id' => $user->id,
            'wheel_id' => $decrypted,
            'full_name' => $user->full_name,
            'address' => $user->address, 'address' => $user->address, 'phone' => $user->phone,
            'post_code' => $user->post_code,
            'key' => $request->wheel_id

        ]);

        $redeem->update([
            'used_at' => Carbon::now()
        ]);

        return ['status' => true, 'message' => 'success', 'key' => $request->wheel_id];
    }
}