<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Reward;
use Illuminate\Http\Request;

class MyRewardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $status = $request->get('status') ? $request->get('status') : 'all';
        $user = $request->user();

        $perPage = 25;

        if (!empty($keyword)) {
            $items = Reward::where('name', 'LIKE', "%$keyword%")->with('user')->with('wheel')
                ->latest()->where(function ($q) use ($status) {
                    if ($status == 'wait') {
                        $q->where('status', 0);
                    }
                    if ($status == 'success') {
                        $q->where('status', 1);
                    }
                })->where('user_id', $user->id)->paginate($perPage);
        } else {
            $items = Reward::where(function ($q) use ($status) {
                if ($status == 'wait') {
                    $q->where('status', 0);
                }
                if ($status == 'success') {
                    $q->where('status', 1);
                }
            })->latest()->with('user')->with('wheel')->where('user_id', $user->id)->paginate($perPage);
        }

        return view('myreward.index', compact('items', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image_url' => 'required'
        ]);
        $requestData = $request->all();
        if ($request->hasFile('image_url')) {
            $requestData['image_url'] = $request->file('image_url')
                ->store('uploads', 'public');
        }

        Reward::create($requestData);

        return redirect('admin/items')->with('flash_message', 'Item added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $item = Reward::findOrFail($id);

        return view('myreward.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = Reward::findOrFail($id);

        return view('admin.items.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();


        $item = Reward::findOrFail($id);
        $item->update([
            'status' => 1
        ]);

        return redirect('admin/rewards')->with('flash_message', 'Item updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Reward::destroy($id);

        return redirect('admin/items')->with('flash_message', 'Item deleted!');
    }
}