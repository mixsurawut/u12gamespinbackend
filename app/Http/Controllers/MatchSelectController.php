<?php

namespace App\Http\Controllers;

use App\MatchSelect;
use Illuminate\Http\Request;

class MatchSelectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = $request->all();
        $m = MatchSelect::create($req);

        return $m;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MatchSelect  $matchSelect
     * @return \Illuminate\Http\Response
     */
    public function show(MatchSelect $matchSelect)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MatchSelect  $matchSelect
     * @return \Illuminate\Http\Response
     */
    public function edit(MatchSelect $matchSelect)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MatchSelect  $matchSelect
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MatchSelect $matchSelect)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MatchSelect  $matchSelect
     * @return \Illuminate\Http\Response
     */
    public function destroy(MatchSelect $matchSelect)
    {
        //
    }
}
