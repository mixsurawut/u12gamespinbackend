<?php

namespace App\Http\Controllers;

use App\Redeem;
use App\Models\Code;

use Illuminate\Http\Request;
use Carbon\Carbon;

class RedeemController extends Controller
{

    public function store(Request $request)
    {
        $user = $request->user();


        $code = Code::whereName($request->name)->first();

        if (!$code) {
            return redirect()->back()->with('msg', 'code not found');
        }

        $check = Redeem::whereCodeId($code->id)->first();
        if ($check) {
            return redirect()->back()->with('msg', 'code is used');
        }

        // $redeem = Redeem::create([
        //     'code_id' => $code->id,
        //     'user_id' => $user->id
        // ]);

        for($i =0 ; $i<$code->credit;$i++){
            $redeem = Redeem::create([
                'code_id' => $code->id,
                'user_id' => $user->id
            ]);
        }

        return redirect()->back()->with('msg', 'success');
    }
}