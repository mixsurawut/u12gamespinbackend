<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\MatchSelect;
use App\Token;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'img_url','full_name','address','post_code','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        // 'scores',
    ];

    // public function getScoresAttribute()
    // {
    //     $m = MatchSelect::where('user_id', $this->id)->sum('score');
    //     return $m * 1;
    // }



    public function getImgUrlAttribute($m)
    {
        $token = Token::whereName('facebook')->first();

        return 'ok';
        // return $m . "&access_token={$token->val}";
    }
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function redeems()
    {
        return $this->hasMany('App\Redeem')->whereNull('used_at');
    }


}
