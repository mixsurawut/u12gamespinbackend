<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{

    protected $fillable = ['user_id', 'wheel_id', 'full_name', 'address', 'post_code', 'phone', 'key', 'status'];

    /**
     * Get the user that owns the Reward
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wheel()
    {
        return $this->belongsTo('App\Models\Wheel');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}