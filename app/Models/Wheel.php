<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Wheel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wheels';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['probability', 'value', 'win', 'result_text', 'item_name', 'item_img'];

    protected $appends = ['type', 'userData'];

    public function getTypeAttribute()
    {
        return "string";
    }
    public function getUserDataAttribute()
    {
        return [
            "id" => Crypt::encryptString($this->id)
        ];
    }
}